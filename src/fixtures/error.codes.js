// All strings must be different
module.exports = {
  // Errors on OTP
  OTPLOG_NOTHING_FOUND: "otp: Nothing Found",
  OTPLOG_LIMIT_CALL: "Error Limit Call",
  PHONE_CONSTRAINT: "otp: Nothing Found",
  CHECK_LOG_ERROR: "Error check Otpcall",
  VERIFY_ERROR: "Error Otpcall",
  REMOVE_VERIFY_ERROR: "Error remove verify",
  TOKEN_ERROR: "Error Otpcall",
  VERIFY_NOTFOUND_ERROR: "not found otp verification",
  USER_NOT_FOUND: "user not found",
  // Unknown Error
  UNKOWN_ERROR: "Unknown Error",
};
