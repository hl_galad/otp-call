const UserModel = require("./UserModel");
const OtpLogModel = require("./OtpLogModel");
const OtpVerificationModel = require("./OtpVerificationModel");
module.exports = {
  User: UserModel,
  OtpLog: OtpLogModel,
  OtpVerification: OtpVerificationModel,
};
