"use strict";

const Sequelize = require("sequelize");

module.exports = {
  name: "otpverification",
  define: {
    id: {
      // id must always exist
      type: Sequelize.UUID, // Uses uuidv4 by default (default value is recommended)
      primaryKey: true,
      defaultValue: Sequelize.UUIDV4,
    },
    userId: {
      type: Sequelize.UUID,
      allowNull: false,
      unique: true,
    },
    phoneNumber: {
      type: Sequelize.STRING(20),
      allowNull: false,
      unique: true,
    },
    trxId: {
      type: Sequelize.STRING(100),
      allowNull: true,
      default: 1,
    },
    token: {
      type: Sequelize.STRING(17),
      allowNull: false,
      unique: true,
    },
    isActive: {
      type: Sequelize.INTEGER(10),
      allowNull: false,
      unique: true,
    },
  },
  options: {
    timestamps: true,
  },
  indexes: [
    {
      unique: "true",
      fields: ["userId", "msisdn"],
    },
  ],
};
