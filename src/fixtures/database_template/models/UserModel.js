"use strict";

const Sequelize = require("sequelize");

// For more information about Sequelize Data Types :
// http://docs.sequelizejs.com/manual/tutorial/models-definition.html#data-types

module.exports = {
  name: "user",
  define: {
    id: {
      // id must always exist
      type: Sequelize.UUID, // Uses uuidv4 by default (default value is recommended)
      primaryKey: true,
      defaultValue: Sequelize.UUIDV4,
    },

    email: {
      type: Sequelize.STRING(200),
      allowNull: false,
      unique: true,
    },

    password: {
      type: Sequelize.TEXT,
      allowNull: true,
    },

    username: {
      type: Sequelize.STRING,
      allowNull: true,
    },

    role: {
      type: Sequelize.STRING,
      defaultValue: "member",
      allowNull: false,
    },

    state: {
      type: Sequelize.STRING(20),
      allowNull: false,
      defaultValue: "pending",
    },

    referralId: {
      type: Sequelize.UUID,
      allowNull: true,
    },

    referralGroupId: {
      type: Sequelize.UUID,
      allowNull: true,
      defaultVale: "default",
    },

    locale: {
      type: Sequelize.STRING(8),
      allowNull: true,
    },

    email_verified: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: false,
    },

    phone_verified: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: false,
    },

    profile_verified: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: false,
    },

    isPhone: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
    },

    data: {
      type: Sequelize.TEXT,
      allowNull: true,
    },

    otp: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      allowNull: false,
    },
    enabled2fa: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
      allowNull: false,
    },
    secret2fa: {
      type: Sequelize.STRING(20),
      defaultValue: "",
      allowNull: true,
    },
  },
  options: {
    timestamps: true,
  },
};
