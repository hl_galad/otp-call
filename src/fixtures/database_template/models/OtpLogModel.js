"use strict";

const Sequelize = require("sequelize");

module.exports = {
  name: "otplog",
  define: {
    id: {
      // id must always exist
      type: Sequelize.UUID, // Uses uuidv4 by default (default value is recommended)
      primaryKey: true,
      defaultValue: Sequelize.UUIDV4,
    },
    userId: {
      type: Sequelize.UUID,
      allowNull: false,
      unique: true,
    },
    phoneNumber: {
      type: Sequelize.STRING(20),
      allowNull: false,
      unique: true,
    },
    countLog: {
      type: Sequelize.INTEGER,
      allowNull: true,
      default: 1,
    },
  },
  options: {
    timestamps: true,
  },
  indexes: [
    {
      unique: "true",
      fields: ["userId"],
    },
  ],
};
