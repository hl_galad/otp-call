"use strict";

const Promise = require("bluebird");
const { MoleculerError } = require("moleculer").Errors;
const CodeTypes = require("../fixtures/error.codes");

// Common methods for request answer to different services
module.exports = {
  methods: {
    requestSuccess(name, data) {
      return Promise.resolve({
        name: name,
        data: data,
        code: 200,
      });
    },
    requestLimitLog(name, data) {
      return Promise.resolve({
        name: name,
        data: data,
        code: 201,
      });
    },
    requestNotExistData(name, data) {
      return Promise.resolve({
        name: name,
        data: data,
        code: 201,
      });
    },

    requestCitCallError(name, data) {
      return Promise.resolve({
        name: name,
        data: data,
        code: 404,
      });
    },
    requestError(codeError) {
      var message, code;
      switch (codeError) {
        // Errors on Table1
        case CodeTypes.OTPLOG_NOTHING_FOUND:
          message = "No entity found in Table1 with the given parameters";
          code = 404;
          break;
        case CodeTypes.PHONE_CONSTRAINT:
          message = "Phone number already used";
          code = 417;
          break;
        case CodeTypes.CHECK_LOG_ERROR:
          message = "error check data verification";
          code = 417;
          break;
        case CodeTypes.TOKEN_ERROR:
          message = "error check data verification";
          code = 417;
          break;
        case CodeTypes.VERIFY_ERROR:
          message = "error check data verification";
          code = 417;
          break;
        case CodeTypes.REMOVE_VERIFY_ERROR:
          message = "error emove verification";
          code = 417;
          break;
        case CodeTypes.VERIFY_NOTFOUND_ERROR:
          message = "error check data verification";
          code = 417;
          break;
        case CodeTypes.USER_NOT_FOUND:
          message = "user not found";
          code = 417;
          break;
        // Unknown Error
        default:
          message = "Operation failed internally: unknown details";
          code = 500;
          break;
      }

      return this.Promise.reject(
        new MoleculerError(codeError, code, "ERR_CRITIAL", {
          code: code,
          message: message,
        })
      );
    },
  },
};
