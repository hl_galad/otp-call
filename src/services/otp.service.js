"use strict";

const Database = require("../adapters/Database");
const Request = require("../mixins/request.mixin");
const CodeTypes = require("../fixtures/error.codes");

const fetch = (...args) =>
  import("node-fetch").then(({ default: fetch }) => fetch(...args));
const configCitCall = {
  apiUrl: "https://citcall.pub/v3/motp",
  apikey:
    "eWpwnexKyZL6cFDfOCHQhtDuPv8CI3j35eKX8ByBoAN4FZqPPwt6BxrP3Zg52KYosCFcCu2",
};

// Filters applied when searching for entities
// Elements correspond to the columns of the table
//
const Filters_Otp = {
  full: ["id", "userId", "phoneNumber", "countLog", "updatedAt"],
  restricted: ["id"],
};
const orderOtp = [["userId", "DESC"]];
const Filters_Verification = {
  full: ["id", "userId", "token", "phoneNumber", "updatedAt"],
  restricted: ["id"],
};
module.exports = {
  name: "otplog",
  mixins: [Request],
  /**
   * sendRequest
   *  @API : otpcall/sendRequest*
   *  @params  : phoneNumber, userId
   * @returns
   */
  actions: {
    sendRequest: {
      params: {
        phoneNumber: "string",
        userId: "string",
      },
      handler(ctx) {
        var userId = ctx.params.userId;
        var phoneNumber = ctx.params.phoneNumber.replace(/\s+/g, "");
        // check DB_verification
        return this.DB_verification.find(ctx, {
          query: {
            userId: userId,
            phoneNumber: phoneNumber,
          },
          filter: Filters_Verification.full,
          order: orderOtp,
        })
          .then((res) => {
            if (res.dataLength > 0) {
              let dataReturn = {};
              dataReturn = {
                wording: "already existing phone",
                dataLength: res.dataLength,
                data: res.data[0].id,
              };
              var queryRemove = {
                id: res.data[0].id,
                userId: userId,
                token: res.data[0].token,
              };

              this.removeExisting(ctx, "DB_verification", queryRemove);
            }
            return this.doCheckOtpLog(ctx);
          })

          .catch(() => {
            return this.requestError(CodeTypes.CHECK_LOG_ERROR);
          });
      },
    },
    /**
     * verifyOtp
     * @API : otpcall/verify
     * @params  : token, phoneNumber, userId
     * @returns
     */
    verifyOtp: {
      params: {
        phoneNumber: "string",
        userId: "string",
        token: "string",
      },
      handler(ctx) {
        const userId = ctx.params.userId;
        const phoneNumber = ctx.params.phoneNumber.replace(/\s+/g, "");
        const getLastToken = ctx.params.token;

        const setQuery = {
          userId: userId,
          phoneNumber: phoneNumber,
          //token: token,
        };

        return this.DB_verification.find(ctx, {
          query: setQuery,
          filter: Filters_Verification.full,
        })
          .then((res) => {
            if (res.dataLength > 0) {
              const fullExistToken = res.data[0].token;
              const splitToken = this.splitToken(fullExistToken);
              const fullReqToken = splitToken.firstToken + getLastToken;

              // check if token existing = request token

              if (fullReqToken === fullExistToken) {
                this.removeExisting(ctx, "DB_verification", setQuery)
                  .then(() => {
                    this.removeExisting(ctx, "DB_otp", setQuery);
                  })
                  .then(() => {
                    this.DB_Users.updateById(ctx, userId, {
                      phone_verified: 1,
                    });
                  })
                  .catch((err) => {
                    return this.requestError(CodeTypes.TOKEN_ERROR);
                  });
                const dataReturn = {
                  userId: res.data[0].userId,
                  phone_verified: 1,
                  phoneNumber: phoneNumber,
                };
                return this.requestSuccess(
                  "success OTP verification",
                  dataReturn
                );
              } else {
                return this.requestError(CodeTypes.VERIFY_NOTFOUND_ERROR);
              }
            } else {
              return this.requestError(CodeTypes.VERIFY_NOTFOUND_ERROR);
            }
          })
          .catch((err) => {
            return this.requestError(CodeTypes.UNKOWN_ERROR);
          });
      },
    },
  },

  methods: {
    getDiffTime(updatedAt) {
      const start = new Date(updatedAt).getTime();
      const end = new Date().getTime();
      const diff = (end - start) / 1000;
      const timeDif = Math.abs(Math.round(diff));
      return timeDif;
    },
    splitToken(token) {
      let lastToken = token.substr(-4);
      let lengthText = token.length;
      let lengthlast = lastToken.length;
      let lengthStart = lengthText - lengthlast;
      let firstToken = token.substring(lengthStart, -1);
      const splitToken = { firstToken: firstToken, lastToken: lastToken };
      return splitToken;
    },

    removeExisting(ctx, dbModel, queryRemove) {
      return Promise.resolve(
        this[dbModel]
          .removeMany(ctx, queryRemove)
          .then(() => {
            return true;
          })
          .catch((err) => {
            return this.requestError(CodeTypes.REMOVE_VERIFY_ERROR);
          })
      );
    },
    removeExistLog(ctx, queryRemove) {
      return Promise.resolve(
        this.DB_otp.removeMany(ctx, queryRemove)
          .then(() => {
            return true;
          })
          .catch((err) => {
            return this.requestError(CodeTypes.REMOVE_VERIFY_ERROR);
          })
      );
    },
    doCheckOtpLog(ctx) {
      var userId = ctx.params.userId;
      var phoneNumber = ctx.params.phoneNumber.replace(/\s+/g, "");
      let dataReturn = {};
      return Promise.resolve(
        // check user db
        this.DB_Users.find(ctx, {
          query: {
            id: userId,
            phone_verified: 0,
          },
          filter: Filters_Otp.restricted,
        })
          .then((res) => {
            // return this.requestSuccess("find user", res);
            if (res.dataLength > 0) {
              return this.DB_otp.find(ctx, {
                query: {
                  userId: userId,
                  phoneNumber: phoneNumber,
                },
                filter: Filters_Otp.full,
                order: orderOtp,
              })
                .then((res) => {
                  // check return data  length
                  if (res.dataLength > 0) {
                    let countLog = res.data[0].countLog;
                    const updatedAt = res.data[0].updatedAt;
                    const timeDiff = this.getDiffTime(updatedAt);
                    const isLimitLog = countLog > 0 ? true : false;
                    const isLimitTime = timeDiff <= 300 ? true : false;

                    // check limit otpcall & time remaining
                    if (isLimitLog && isLimitTime) {
                      // load API/
                      dataReturn = {
                        wording: "over limit call on",
                        countLog: countLog,
                        dateDiff: timeDiff,
                        isLimitLog: isLimitLog,
                        isLimitTime: isLimitTime,
                      };
                      return this.requestLimitLog(
                        "over limit call",
                        dataReturn
                      );
                    } else {
                      return this.doUpdateOtpLog(ctx, countLog + 1)
                        .then(() => {
                          return this.loadApiCitCal(ctx);
                        })
                        .catch((err) => {
                          return this.requestError(CodeTypes.UNKOWN_ERROR);
                        });

                      // load API/ this.loadApiCitCal(ctx);
                    }
                  } else {
                    return this.doInsertOtpLog(ctx).then(() => {
                      return this.loadApiCitCal(ctx);
                    });
                  }
                })
                .catch((err) => {
                  if (
                    err.name === "Database Error" &&
                    Array.isArray(err.data)
                  ) {
                    if (
                      err.data[0].type === "unique" &&
                      err.data[0].field === "phoneNumber"
                    )
                      return this.requestError(CodeTypes.PHONE_CONSTRAINT);
                  }

                  return this.requestError(CodeTypes.UNKOWN_ERROR);
                });
            } else {
              return this.requestNotExistData("user not found", res);
            }
          })
          .catch((err) => {
            return this.requestError(CodeTypes.USER_NOT_FOUND);
          })
      );
    },
    doUpdateOtpLog(ctx, countLogNew) {
      var userId = ctx.params.userId;
      var phoneNumber = ctx.params.phoneNumber.replace(/\s+/g, "");
      return Promise.resolve(
        this.DB_otp.updateMany(
          ctx,
          {
            userId: userId,
            phoneNumber: phoneNumber,
          },
          {
            countLog: countLogNew,
          }
        )
          .then(() => {
            let res = true;
            return res;
          })
          .catch((error) => {
            return error;
          })
      );
    },
    doInsertOtpLog(ctx) {
      return Promise.resolve(
        this.DB_otp.insert(ctx, {
          userId: ctx.params.userId,
          phoneNumber: ctx.params.phoneNumber.replace(/\s+/g, ""),
          countLog: 1,
        }).then(() => {
          let res = true;
          return res;
        })
      );
    },
    doRemoveOtpLog(ctx) {
      return Promise.resolve(
        this.DB_otp.insert(ctx, {
          userId: ctx.params.userId,
          phoneNumber: ctx.params.phoneNumber.replace(/\s+/g, ""),
          countLog: ctx.params.countLog,
        }).then(() =>
          this.requestSuccess(
            "remove success",
            ctx.params.phoneNumber.replace(/\s+/g, "")
          )
        )
      );
    },
    doInsertOtpVerify(ctx, data) {
      return Promise.resolve(
        this.DB_verification.insert(ctx, data).then(() => {
          const strToken = this.splitToken(data.token);

          const dataReturn = {
            firstToken: strToken.firstToken,
            userId: data.userId,
          };

          return this.requestSuccess("Insert otp verify", dataReturn);
        })
      );
    },

    async loadApiCitCal(ctx) {
      // load AXIOS API CitCall
      const body = {
        msisdn: ctx.params.phoneNumber.replace(/\s+/g, ""),
        gateway: ctx.params.gateway,
      };

      const headers = {
        accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Apikey " + configCitCall.apikey,
      };
      const response = await fetch(configCitCall.apiUrl, {
        method: "post",
        body: JSON.stringify(body),
        headers: headers,
      });
      const dataGet = await response.json();
      //ctx.params.phoneNumber = data.msisdn

      if (dataGet.rc == 0) {
        const dataInsert = {
          userId: ctx.params.userId,
          phoneNumber: ctx.params.phoneNumber.replace(/\s+/g, ""),
          trxId: dataGet.trxid,
          token: dataGet.token,
          isActive: 1,
        };
        return this.doInsertOtpVerify(ctx, dataInsert);
      } else if (dataGet.rc == 98) {
        return this.doUpdateOtpLog(ctx, 0).then(() => {
          return this.requestCitCallError("Authorization failed", dataGet);
        });
      } else if (dataGet.rc == 34) {
        return this.doUpdateOtpLog(ctx, 0).then(() => {
          return this.requestCitCallError(
            "Service temporary unavilable",
            dataGet
          );
        });
      } else {
        return this.requestError(CodeTypes.UNKOWN_ERROR);
      }
    },
  },

  created() {
    this.DB_otp = new Database("OtpLog", Filters_Otp.restricted);
    this.DB_Users = new Database("User");
    this.DB_verification = new Database("OtpVerification");
  },
};
