"use strict";

const Promise = require("bluebird");
const ApiGateway = require("moleculer-web");
const { MoleculerError } = require("moleculer").Errors;
const Request = require("../mixins/request.mixin");
const CodeTypes = require("../fixtures/error.codes");

module.exports = {
  name: "otpCallApi",
  mixins: [ApiGateway, Request],
  settings: {
    port: process.env.PORT || 3000,

    cors: {
      origin: "*",
      methods: ["POST"],
      allowedHeaders: ["Content-Type"],
      exposedHeaders: [],
      credentials: false,
      maxAge: 3600,
    },

    //path: "/api",

    routes: [
      {
        bodyParsers: {
          json: true,
        },
        path: "/otpcall/",
        authorization: false,
        whitelist: ["otplog.sendRequest", "otplog.verifyOtp"],
        aliases: {
          "POST sendRequest": "otplog.sendRequest",
          "POST verify": "otplog.verifyOtp",
        },
      },
    ],
  },

  methods: {},
};
