# Install

```
# Install dependencies
npm install
```

# otp-call

Moleculer for citcall miscall otp.

**sendRequest_Otp**

- route : localhost:3000/otpcall/sendRequest
- service : otp.service
- action : sendRequest
- params: {
  phoneNumber: "string",
  userId: "string",
  },

**sendRequest_Otp_Mechanism**

- check otpverifications (remove if exist)
- doCheckOtpLog :

  > check user by {id: userId, phone_verified:0} (if user not exist : return user not found).
  > check exist Otp log by { userId: userId,phoneNumber: phoneNumber,}
  > if exist otp log : check countlog & time latest log
  > return respon "limit otp call" if isLimitLog & isLimitTime is true
  > doInsertOtpLog
  > laod loadApiCitCal if not exist otp log & not limit otp log

  - loadApiCitCal

    > if (dataGet.rc == 0) :

    - insert to otpverifications
    - params: { userId: ctx.params.userId,
      phoneNumber: ctx.params.phoneNumber.replace(/\s+/g, ""),
      trxId: dataGet.trxid,
      token: dataGet.token,
      isActive: 1,}

  - return requestSuccess
  - {firstToken: firstToken, userId: userId}
  - firstToken= token - last 4 digits from the number that
    miscalled

> if (dataGet.rc == 98) :

    - update count otplog =0
    - return Authorization failed

> if (dataGet.rc == 34) :

    - update count otplog =0
    - return Service temporary unavilabl

**verify_Otp**

- route : localhost:3000/otpcall/verify
- service : otp.service
- action : verifyOtp
- params: {
  phoneNumber: "string",
  userId: "string",
  token:"string" (last 4 digits from the number that
  miscalled),
  },

**verify_Otp_Mechanism**

- check otpverifications

  > if data exist :

  - set res token as fullExistToken
  - split fullExistToken to get firstToken
  - set firstToken + ctx.params.token as fullReqToken
    > if fullReqToken == fullExistToken :
  - remove existing otpverifications by userId & phoneNumber
  - remove existing otp by userId & phoneNumbe
  - update user set phone_verified=1
  - return success OTP verification

  > if data not exist :

  - return not found otp verification
